package com.invisiphoto;

import java.io.File;
import java.io.IOException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.View;
import android.view.Window;

import rx.android.view.ViewObservable;

import static rx.android.view.ViewObservable.clicks;

public class SelectImageDialog extends Dialog {

	protected static final String TAG = SelectImageDialog.class.getName();
	private Main mActivity;

	public SelectImageDialog(Main activity) {
		super(activity);
		mActivity = activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.select_image_dialog);

		clicks(findViewById(R.id.btn_cancel)).subscribe(onClickEvent -> cancel());
		
		PackageManager pm = mActivity.getPackageManager();
		if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			findViewById(R.id.btn_camera).setVisibility(View.GONE);
		}

		clicks(findViewById(R.id.btn_camera)).subscribe(onClickEvent -> {
			cancel();
			String storageState = Environment
					.getExternalStorageState();
			if (storageState.equals(Environment.MEDIA_MOUNTED)) {

				String imageDirectoryPath = Environment
						.getExternalStorageDirectory().getName()
						+ File.separator
						+ "Android"
						+ File.separator
						+ "data"
						+ File.separator
						+ mActivity.getPackageName()
						+ File.separator
						+ "snapshot";

				String fileName = "temp.jpg";

				File imageFile = new File(imageDirectoryPath
						+ File.separator + fileName);
				try {
					imageFile.getParentFile().mkdirs();
					if (imageFile.exists() == false) {
						imageFile.createNewFile();
					} else {
						imageFile.delete();
						imageFile.createNewFile();
					}
				} catch (IOException e) {
					Log.e(TAG, "Could not create file.", e);
				}
				Log.i(TAG, imageDirectoryPath);

				Intent takeImageFromCameraIntent = new Intent(
						MediaStore.ACTION_IMAGE_CAPTURE);

				ContentValues values = new ContentValues();
				values.put(Media.TITLE, "DownloadedImage");
				values.put(Media.BUCKET_ID,
						imageDirectoryPath.hashCode());
				values.put(Media.BUCKET_DISPLAY_NAME,
						imageDirectoryPath);

				values.put(Media.MIME_TYPE, "image/jpeg");
				values.put(Media.DESCRIPTION,
						"DownloadedImage capture by camera");
				values.put(Media.DATA, imageFile.getAbsolutePath());

				Uri imageUri = mActivity.getContentResolver().insert(
						Media.EXTERNAL_CONTENT_URI, values);
				takeImageFromCameraIntent.putExtra(
						android.provider.MediaStore.EXTRA_OUTPUT,
						imageUri);
				((Main) mActivity).setSnapshotUri(imageUri);
				mActivity.startActivityForResult(takeImageFromCameraIntent,
						Main.ACTION_TAKE_FROM_CAMERA);

			} else {
				new AlertDialog.Builder(mActivity)
						.setMessage(R.string.error_no_sd_card)
						.setCancelable(true).create().show();
			}
		});

		clicks(findViewById(R.id.btn_gallery)).subscribe(onClickEvent -> {
			cancel();
			Intent intent = new Intent(Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			mActivity.startActivityForResult(intent, Main.ACTION_TAKE_FROM_GALLERY);
		});

		clicks(findViewById(R.id.btn_secure_vault)).subscribe(onClickEvent -> {
			cancel();
			mActivity.openSecureStore();
		});
	}
}