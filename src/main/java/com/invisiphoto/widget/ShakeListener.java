package com.invisiphoto.widget;

import android.view.animation.Animation;

public abstract class ShakeListener implements Animation.AnimationListener {
	@Override public void onAnimationStart(Animation animation) {}
	@Override public void onAnimationRepeat(Animation animation) {}	
}
