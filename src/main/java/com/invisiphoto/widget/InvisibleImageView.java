package com.invisiphoto.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class InvisibleImageView extends ImageView {

	private boolean show;

	public InvisibleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public InvisibleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (show) {
			super.onDraw(canvas);
		}
		// TODO draw circles
		
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			show = true;
			invalidate();
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			show = false;
			invalidate();
		}
		return true;
	}
}
