package com.invisiphoto.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class Pin extends EditText {

	private static final String TAG = Pin.class.getSimpleName();

	private Paint white;
	private Paint black;
	private int mSize;
	private OnPinInputDoneListener mListener;

	private Activity mActivityToFinish;

	public Pin(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public Pin(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public Pin(Context context) {
		super(context);
		init();
	}

	private void init() {

		setInputType(EditorInfo.TYPE_CLASS_PHONE);

		addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == mSize) {
					mListener.onPinInputDone(Pin.this, s.toString());
				}
			}
		});

		white = new Paint();
		white.setColor(Color.WHITE);
		white.setStyle(Paint.Style.FILL);

		black = new Paint();
		black.setColor(Color.BLACK);
		black.setStyle(Paint.Style.FILL);

		mSize = 4;
	}

	@Override
	public void draw(Canvas canvas) {

		int width = getMeasuredWidth();

		int separatorSize = width / (mSize * 4 - 1);
		int rectWidth = (width - (mSize - 1) * separatorSize) / mSize;
		int rectHeight = rectWidth;

		for (int i = 0; i < mSize; i++) {
			int top = 0;
			int bottom = rectHeight;
			int left = (separatorSize + rectWidth) * i;
			int right = left + rectWidth;
			canvas.drawRect(left, top, right, bottom, white);
			if (i < getText().length()) {
				int radius = (top + bottom) / 5;
				int cx = (left + right) / 2;
				int cy = (top + bottom) / 2;
				// draw circle
				canvas.drawCircle(cx, cy, radius, black);
			}
		}
	}

	public void clearText() {
		setText("");
		invalidate();
	}

	public void shake(final ShakeListener listener) {
		invalidate();
		final long millis = 500;
		Animation animation = new ShakeAnimation(millis);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				listener.onAnimationStart(animation);
				Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
				v.vibrate(millis);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				listener.onAnimationRepeat(animation);
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				listener.onAnimationEnd(animation);
			}
		});
		startAnimation(animation);
	}

	private class ShakeAnimation extends Animation {

		public ShakeAnimation(long millis) {
			setDuration(millis);
		}

		public void applyTransformation(float interpolatedTime, Transformation t) {
			t.getMatrix().reset();
			t.getMatrix().postTranslate((float) Math.random() * 6 - 3, (float) Math.random() * 6 - 3);
		}
	}

	public void setOnPinInputDoneListener(OnPinInputDoneListener listener) {
		mListener = listener;
	}

	public interface OnPinInputDoneListener {
		void onPinInputDone(Pin pin, String text);
	}

	@Override
	protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
		super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
		final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		if (gainFocus) {
			imm.showSoftInput(Pin.this, 0);
		} else {
			imm.hideSoftInputFromWindow(getWindowToken(), 0);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(this, 0);
		return super.onTouchEvent(event);
	}

	public void showKeyboard() {
		final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		postDelayed(new Runnable() {

			@Override
			public void run() {
//				imm.showSoftInput(Pin.this, 0);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
			}
		}, 200);
	}
	
	public void hideKeyboard() {
		final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindowToken(), 0);
	}

	public void setActivityToFinishOnBack(Activity activity) {
		mActivityToFinish = activity;
	}

	@Override
	public boolean dispatchKeyEventPreIme(KeyEvent event) {
		if (mActivityToFinish != null && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			KeyEvent.DispatcherState state = getKeyDispatcherState();
			if (state != null) {
				if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
					state.startTracking(event, this);
					return true;
				} else if (event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled() && state.isTracking(event)) {
					InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getWindowToken(), 0);
					mActivityToFinish.onBackPressed();
					return true;
				}
			}
		}
		return super.dispatchKeyEventPreIme(event);
	}
}