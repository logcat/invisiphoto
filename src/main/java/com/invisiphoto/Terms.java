package com.invisiphoto;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

public class Terms extends Activity {
	
	private static final String ACCEPTED = "accepted";
	private SharedPreferences mPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPreferences = getPreferences(MODE_PRIVATE);
		
		if (mPreferences.getBoolean(ACCEPTED, false)) {
			startMain();
		}
		
		setContentView(R.layout.terms);
		
		((TextView) findViewById(R.id.header)).setText(R.string.terms_of_use);
		
		TextView terms = (TextView) findViewById(R.id.terms_text);
		String termsText = getResources().getString(R.string.terms_text);
		terms.setText(termsText);
		terms.setMovementMethod(new ScrollingMovementMethod());
	}
	
	public void onAcceptClick(View view) {
		mPreferences.edit().putBoolean(ACCEPTED, true).commit();
		startMain();
	}
	
	public void startMain() {
		startActivity(new Intent(this, Main.class));
		finish();
	}
	
	public void onDeclineClick(View view) {
		finish();
	}
}