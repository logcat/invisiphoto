package com.invisiphoto;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;

public class SaveDialog extends Dialog {
	
	private Button mButtonNo;

	private Button mButtonYes;

	public SaveDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.save_dialog);
		mButtonNo = (Button) findViewById(R.id.btn_no);
		mButtonYes = (Button) findViewById(R.id.btn_yes);
	}
	
	public Button getButtonNo() {
		return mButtonNo;
	}

	public Button getButtonYes() {
		return mButtonYes;
	}	

}