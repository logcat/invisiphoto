package com.invisiphoto;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.invisiphoto.utils.PrefsUtils;
import com.invisiphoto.widget.Pin;
import com.invisiphoto.widget.Pin.OnPinInputDoneListener;
import com.invisiphoto.widget.ShakeListener;

public class PincodeSettings extends FragmentActivity {

	private Pin mPin;
	private ViewFlipper mViewFlipper;
	private PrefsUtils mPrefs;
	private Pin mPinNewConfirm;
	private Pin mPinNew;

	private String mPinNewText;

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.pincode_settings);

		mPrefs = ((InvApplication) getApplication()).getPrefs();

		((TextView) findViewById(R.id.header)).setText(R.string.password);

		mViewFlipper = (ViewFlipper) findViewById(R.id.flipper);
		mViewFlipper.setOutAnimation(outToLeftAnimation());
		mViewFlipper.setInAnimation(inFromRightAnimation());
		
		mPin = (Pin) findViewById(R.id.pin);
		mPin.setActivityToFinishOnBack(this);
		mPinNew = (Pin) findViewById(R.id.pinnew);
		mPinNew.setActivityToFinishOnBack(this);
		mPinNewConfirm = (Pin) findViewById(R.id.pinnewrepeat);
		mPinNewConfirm.setActivityToFinishOnBack(this);
		
		if (!mPrefs.hasPassword()) {
			mViewFlipper.showNext();
		}

		mPin.setOnPinInputDoneListener(new OnPinInputDoneListener() {
			@Override
			public void onPinInputDone(final Pin pin, String text) {
				if (mPrefs.matchPassword(text)) {
					mViewFlipper.showNext();
				} else {
					pin.shake(new ShakeListener() {
						@Override
						public void onAnimationEnd(Animation animation) {
							pin.clearText();
						}
					});
				}
			}
		});

		mPinNew.setOnPinInputDoneListener(new OnPinInputDoneListener() {
			@Override
			public void onPinInputDone(Pin pin, String text) {
				mPinNewText = text;
				mViewFlipper.showNext();
			}
		});

		mPinNewConfirm.setOnPinInputDoneListener(new OnPinInputDoneListener() {
			@Override
			public void onPinInputDone(Pin pin, String text) {
				if (mPinNewText.equals(text)) {
					mPrefs.setPassword(text);
					finish();
				} else {
					mPinNewConfirm.shake(new ShakeListener() {
						@Override
						public void onAnimationEnd(Animation animation) {
							mPinNewConfirm.clearText();
						}
					});
				}
			}
		});
	};

	protected Animation inFromRightAnimation() {

		Animation inFromRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromRight.setDuration(500);
		inFromRight.setInterpolator(new AccelerateInterpolator());
		return inFromRight;
	}

	protected Animation outToLeftAnimation() {
		Animation outtoLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoLeft.setDuration(500);
		outtoLeft.setInterpolator(new AccelerateInterpolator());
		return outtoLeft;
	}

	@Override
	protected void onResume() {
		super.onResume();
		mPin.showKeyboard();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mPin.hideKeyboard();
	}
}