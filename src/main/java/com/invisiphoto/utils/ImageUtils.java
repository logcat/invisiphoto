package com.invisiphoto.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;

import com.bumptech.glide.Glide;

import net.iharder.Base64;

import rx.Observable;
import rx.util.async.Async;

public class ImageUtils {

	private static final int IMAGE_MAX_WIDTH = 640;

	public static byte[] convertBitmapToByteArray(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return baos.toByteArray();
	}

	public static String base64(Bitmap bitmap) {
		return Base64.encodeBytes(convertBitmapToByteArray(bitmap));
	}

	public static Bitmap createBitmapFromFile(File file) throws IOException {
		// reducing size
		InputStream is = new FileInputStream(file);
		
		// Decode image size
		BitmapFactory.Options o = getBoundsOptions(is);
		is.close();

		int imageMaxWidth = IMAGE_MAX_WIDTH;

		int imageMaxHeight = imageMaxWidth * o.outHeight / o.outWidth;

		boolean rotate = o.outHeight < o.outWidth;
		if (rotate) {
			imageMaxHeight = IMAGE_MAX_WIDTH;
			imageMaxWidth = imageMaxHeight * o.outWidth / o.outHeight;
		}

		BitmapFactory.Options o2 = getScaledOptions(o, imageMaxHeight);

		is = new FileInputStream(file);
		// get a little bit large then 640 wide
		Bitmap bitmap = BitmapFactory.decodeStream(is, null, o2);
		is.close();
		// resize to 640 exactly
		bitmap = Bitmap.createScaledBitmap(bitmap, imageMaxWidth,
				imageMaxHeight, true);
		// rotate if we need it
		bitmap = rotateBitmap(rotate, bitmap);
		return bitmap;
	}

	public static Bitmap createBitmapFromUri(Uri imageUri, Context context)
			throws IOException {
		// reducing size
		InputStream is = context.getContentResolver().openInputStream(imageUri);

		// Decode image size
		BitmapFactory.Options o = getBoundsOptions(is);
		is.close();

		int imageMaxWidth = IMAGE_MAX_WIDTH;

		int imageMaxHeight = imageMaxWidth * o.outHeight / o.outWidth;

		boolean rotate = o.outHeight < o.outWidth;
		if (rotate) {
			imageMaxHeight = IMAGE_MAX_WIDTH;
			imageMaxWidth = imageMaxHeight * o.outWidth / o.outHeight;
		}

		BitmapFactory.Options o2 = getScaledOptions(o, imageMaxHeight);

		is = context.getContentResolver().openInputStream(imageUri);
		// get a little bit large then 640 wide
		Bitmap bitmap = BitmapFactory.decodeStream(is, null, o2);
		is.close();
		// resize to 640 exactly
		bitmap = Bitmap.createScaledBitmap(bitmap, imageMaxWidth,
				imageMaxHeight, true);
		// rotate if we need it
		bitmap = rotateBitmap(rotate, bitmap);
		return bitmap;
	}

	public static Bitmap rotateBitmap(boolean rotate, Bitmap bitmap) {
		if (rotate) {
			Matrix matrix = new Matrix();
			matrix.setRotate(90);
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
					bitmap.getHeight(), matrix, true);
		}
		return bitmap;
	}

	public static BitmapFactory.Options getScaledOptions(
			BitmapFactory.Options o, int imageMaxHeight) {
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		if (o.outHeight > imageMaxHeight || o.outWidth > IMAGE_MAX_WIDTH) {
			// Decode with inSampleSize
			o2.inSampleSize = (int) Math.pow(
					2,
					(int) Math.round(Math.log(IMAGE_MAX_WIDTH
							/ (double) Math.max(o.outHeight, o.outWidth))
							/ Math.log(0.5)));
		}
		return o2;
	}

	public static BitmapFactory.Options getBoundsOptions(InputStream is) {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		BitmapFactory.decodeStream(is, null, o);
		return o;
	}

	public static Observable<byte[]> downloadImage(String link, Context context) {
		return Async.fromCallable(new Callable<byte[]>() {
			@Override
			public byte[] call() throws Exception {
				Bitmap bitmap = Glide.with(context).load(link).asBitmap().into(-1, -1).get();
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
				return outputStream.toByteArray();
			}
		});
	}

}
