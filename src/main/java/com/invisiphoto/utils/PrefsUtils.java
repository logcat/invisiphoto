package com.invisiphoto.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsUtils {

	public static final String PREFS = "prefs";
	
	public static final String ACCEPTED = "accepted";

	public static final String PASS_HASH = "ph";

	private SharedPreferences mPrefs;

	public PrefsUtils(Context context) {
		mPrefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
	}

	public boolean matchPassword(String s) {
		return SHA1.sha1(s).equals(mPrefs.getString(PASS_HASH, ""));
	}

	public void setPassword(String s) {
		mPrefs.edit().putString(PASS_HASH, SHA1.sha1(s)).commit();
	}
	
	public boolean hasPassword() {
		return mPrefs.getString(PASS_HASH, "").length() > 0;
	}
	
	public void setAccepted(boolean accepted) {
		mPrefs.edit().putBoolean(ACCEPTED, accepted).commit();
	}
	
	public boolean isAccepted() {
		return mPrefs.getBoolean(ACCEPTED, false);
	}
}