package com.invisiphoto.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.invisiphoto.InvApplication;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import rx.util.async.Async;

public class SavedImagesDbAdapter {
	
	private static final String _ID = "_id";
	public static final String HASH = "hash";
	public static final String DATA = "image";

	private static final String TABLE_NAME = "saved_images";

	public static final String DATABASE_CREATE = "create table " + TABLE_NAME
			+ " (" +  _ID + " integer primary key autoincrement, " + HASH + " string not null, " 
			+ DATA + " BLOB not null);";


	private SQLiteDatabase mDb;

	public SavedImagesDbAdapter(Context context) {
		mDb = InvApplication.getDb(context);
	}

	public Observable<Void> insert(SavedImage image) {
		return Async.start(() -> {
				ContentValues cv = new ContentValues();
				cv.put(HASH, image.hash);
				cv.put(DATA, image.data);

				mDb.insert(TABLE_NAME, null, cv);
				return null;
		}, Schedulers.io());
	}


	// TODO use rx sqlite wrappers 
	public Observable<SavedImage> read(String hash) {
		return Async.start(() -> {
			Cursor c = mDb.query(TABLE_NAME, new String[] { HASH, DATA}, HASH
					+ "=?", new String[] { hash }, null, null, null);
			try {
				if (c.moveToFirst()) {
					byte[] data = c.getBlob(c.getColumnIndex(DATA));
					return new SavedImage(hash, data);
				}
			} finally {
				c.close();
			}
			return null;
		}, Schedulers.io());
	}

	public Cursor readAll() {
		return mDb.query(TABLE_NAME, new String[] {_ID, HASH}, null, null, null, null, null);
	}
	
	public Observable<Integer> delete(String hash) {
		return Async.start(() -> mDb.delete(TABLE_NAME, HASH + "=?" , new String[]{ hash }), Schedulers.io());
	}

}