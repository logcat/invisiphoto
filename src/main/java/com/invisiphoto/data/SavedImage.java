package com.invisiphoto.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class SavedImage {

	public String hash;

	public byte[] data;
	
	public SavedImage(String hash, byte[] data) {
		this.hash = hash;
		this.data = data;
	}
	
	public Bitmap getBitmap() {
		return BitmapFactory.decodeByteArray(data, 0, data.length);
	}

}
