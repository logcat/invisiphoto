package com.invisiphoto.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class DownloadedImage {
	
	public DownloadedImage(String hash, long interval, long timeCreated, byte[] data) {
		this.hash = hash;
		this.interval = interval;
		this.timeCreated = timeCreated;
		this.data = data;
	}
	
	public String hash;
	public long interval;
	public long timeCreated;
	public byte[] data;
	
	public Bitmap getBitmap() {
		return BitmapFactory.decodeByteArray(data, 0, data.length);
	}

}