package com.invisiphoto.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;
import android.util.Log;

import com.invisiphoto.InvApplication;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.schedulers.Schedulers;
import rx.util.async.Async;

public class DownloadedImagesDbAdapter {

	public static final String HASH = "hash";
	public static final String INTERVAL = "interval";
	public static final String DATA = "image";
	public static final String TIME_CREATED = "tcreated";

	private static final String TABLE_NAME = "images";

	public static final String DATABASE_CREATE = "create table " + TABLE_NAME
			+ " (_id integer primary key autoincrement, " + HASH + " string not null, " + INTERVAL
			+ " numeric not null, " + TIME_CREATED + " numeric not null, " + DATA
			+ " BLOB not null);";


	private SQLiteDatabase mDb;

	public DownloadedImagesDbAdapter(Context context) {
		mDb = InvApplication.getDb(context);
	}

	public void insert(DownloadedImage image) {
		Async.start(() -> {
			ContentValues cv = new ContentValues();
			cv.put(HASH, image.hash);
			cv.put(INTERVAL, image.interval);
			cv.put(DATA, image.data);
			cv.put(TIME_CREATED, System.currentTimeMillis());

			mDb.insert(TABLE_NAME, null, cv);
			return null;
		}, Schedulers.io());
	}

	public Observable<DownloadedImage> read(String hash) {
		return Async.start(() -> {
			Cursor c = mDb.query(TABLE_NAME, new String[] { HASH, DATA, INTERVAL, TIME_CREATED }, HASH
					+ "=?", new String[] { hash }, null, null, null);
			try {
				if (c.moveToFirst()) {
					byte[] data = c.getBlob(c.getColumnIndex(DATA));
					long interval = c.getLong(c.getColumnIndex(INTERVAL));
					long timeCreated = c.getLong(c.getColumnIndex(TIME_CREATED));
					return new DownloadedImage(hash, interval, timeCreated, data);
				}
			} finally {
				c.close();
			}
			return null;
		});
	}

	public Observable<Boolean> contains(String hash) {
		return Async.start(() -> {
			Cursor c = mDb.query(TABLE_NAME, new String[] { HASH }, HASH + " = ?",
					new String[] { hash }, null, null, null);
			boolean result = c.moveToFirst();
			c.close();
			Log.d(TABLE_NAME, "contains " + hash + " : " + result);
			return result;
		}, Schedulers.io());
	}

	public void removeOutdatedImages() {
		Async.start(() -> {
			int i = mDb.delete(TABLE_NAME, TIME_CREATED + " + " + INTERVAL + " < " + System.currentTimeMillis(), null);
			Log.d(TABLE_NAME, "deleted count: " + i);
			return i;
		}, Schedulers.io());
	}

	public Observable<Boolean> saveToDb(String hash, String intervalString, byte[] data) {
		return Async.fromCallable(new Callable<Boolean>() {
			@Override
			public Boolean call() throws Exception {
				insert(new DownloadedImage(hash, Long
						.parseLong(intervalString) * 1000, System
						.currentTimeMillis(), data));
				return true;
			}
		});
	}
}