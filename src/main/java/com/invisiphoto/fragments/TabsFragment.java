package com.invisiphoto.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.invisiphoto.Main;
import com.invisiphoto.PickMessageMethodDialog2;
import com.invisiphoto.PickTimeDialog;
import com.invisiphoto.PincodeSettings;
import com.invisiphoto.R;
import com.invisiphoto.SelectImageDialog;

import rx.Observable;
import rx.android.view.OnClickEvent;
import rx.android.view.ViewObservable;
import rx.functions.Action1;

public class TabsFragment extends Fragment {

    private static final String TAG = "FragmentTabs";

    public static final String TAB_TIMER = "timer";
    public static final String TAB_ENVELOPE = "enveloper";
    public static final String TAB_TAKE_PICTURE = "take_picture";
    public static final String TAB_SETTINGS = "settings";
    public static final String TAB_SAVE = "save";

    private View mRoot;
    private TabHost mTabHost;

    private Main mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.tabs_fragment, null);
        mTabHost = (TabHost) mRoot.findViewById(android.R.id.tabhost);
        mActivity = (Main) getActivity();
        setupTabs();
        return mRoot;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    private void setupTabs() {
        mTabHost.setup(); // important!
        mTabHost.addTab(newTab(TAB_TIMER, R.drawable.timer_selector));
        mTabHost.addTab(newTab(TAB_ENVELOPE, R.drawable.envelope_selector));
        mTabHost.addTab(newTab(TAB_TAKE_PICTURE, R.drawable.take_picture_selector));
        mTabHost.addTab(newTab(TAB_SAVE, R.drawable.save_selector));
        mTabHost.addTab(newTab(TAB_SETTINGS, R.drawable.settings_selector));
        updateTabsState(); // disable tabs
        updateTabsListeners(); // handle tab clicks impllicitly
    }

    private void updateTabsListeners() {
        clicks(TAB_TIMER).subscribe(onClickEvent
                -> new PickTimeDialog(mActivity).show());
        clicks(TAB_ENVELOPE).subscribe(onClickEvent
                -> new PickMessageMethodDialog2(mActivity).show());
        clicks(TAB_TAKE_PICTURE).subscribe(onClickEvent
                -> new SelectImageDialog(mActivity).show());
        clicks(TAB_SAVE).subscribe(onClickEvent
                -> mActivity.openSecureStore());
        clicks(TAB_SETTINGS).subscribe(onClickEvent
                -> startActivity(new Intent(mActivity, PincodeSettings.class)));
    }

    private Observable<OnClickEvent> clicks(String tag) {
        return ViewObservable.clicks(mTabHost.getTabWidget().findViewWithTag(tag));
    }

    public void updateTabsState() {
        enableTab(TAB_TIMER, mActivity.doWeHaveImage());
        enableTab(TAB_ENVELOPE, mActivity.doWehaveImageAndTime());
    }

    private void enableTab(String tag, boolean enabled) {
        mTabHost.getTabWidget().findViewWithTag(tag).setEnabled(enabled);
        mTabHost.getTabWidget().findViewWithTag(tag).setPressed(!enabled);
    }

    private TabSpec newTab(String tag, int drawableId) {
        Log.d(TAG, "buildTab(): tag=" + tag);

        ImageView indicator = (ImageView) LayoutInflater.from(mActivity).inflate(
                R.layout.tab_item,
                (ViewGroup) mRoot.findViewById(android.R.id.tabs), false);

        indicator.setImageDrawable(getResources().getDrawable(drawableId));
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f);
        indicator.setLayoutParams(params);
        indicator.setTag(tag);

        TabSpec tabSpec = mTabHost.newTabSpec(tag);
        tabSpec.setIndicator(indicator);
        tabSpec.setContent(android.R.id.tabcontent);
        return tabSpec;
    }

}
