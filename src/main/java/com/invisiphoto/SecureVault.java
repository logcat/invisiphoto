package com.invisiphoto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.invisiphoto.data.SavedImage;
import com.invisiphoto.data.SavedImagesDbAdapter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.invisiphoto.Main.applySchedulers;

public class SecureVault extends Activity {
	
	private SavedImagesDbAdapter mSavedImagesDbAdapter;
	private boolean isEditMode;
	private ImageAdapter mImageAdapter;
	private GridView gridview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.secure_vault);

		isEditMode = false;
		
		mSavedImagesDbAdapter = new SavedImagesDbAdapter(this);
		
		gridview = (GridView) findViewById(R.id.gridview);
		mImageAdapter = new ImageAdapter(this, mSavedImagesDbAdapter.readAll());
	    gridview.setAdapter(mImageAdapter);
	    gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String hash = view.findViewById(R.id.image).getTag().toString();
				if (isEditMode) {
					mSavedImagesDbAdapter.delete(hash);
					mImageAdapter.changeCursor(mSavedImagesDbAdapter.readAll());
				} else {
					Intent intent = new Intent();
					intent.putExtra(SavedImagesDbAdapter.HASH, hash);
					setResult(RESULT_OK, intent);
					finish();
				}
			}
		});
	}
	
	public class ImageAdapter extends CursorAdapter {

		private LayoutInflater mInflater;

		public ImageAdapter(Context context, Cursor c) {
			super(context, c);
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View contentView, Context context, Cursor c) {
			ImageView deleteView = (ImageView) contentView.findViewById(R.id.delete);
			if (isEditMode) {
				deleteView.setVisibility(View.VISIBLE);
			} else {
				deleteView.setVisibility(View.GONE);
			}
			
			int imageHeigth = (int) (getWindow().getDecorView().getWidth() / 2 * 1.33f); // 640/480 is wide market ratio
			contentView.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.WRAP_CONTENT, imageHeigth));
			

			mSavedImagesDbAdapter.read(c.getString(c.getColumnIndex(SavedImagesDbAdapter.HASH)))
					.compose(applySchedulers())
					.subscribe(savedImage -> {
						ImageView imageView = (ImageView) contentView.findViewById(R.id.image);
						imageView.setTag(savedImage.hash);
						imageView.setImageBitmap(savedImage.getBitmap());
					});

		}

		@Override
		public View newView(Context context, Cursor c, ViewGroup parent) {
			return mInflater.inflate(R.layout.image, null);
		}
	}

	private int convertDpToPx(int dp) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}
	
	public void onEditClick(View view) {
		isEditMode = !isEditMode;
		if (isEditMode) {
			((Button) view).setText(R.string.done);
		} else {
			((Button) view).setText(R.string.edit);
		}
		mImageAdapter.notifyDataSetChanged();
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}
}
