package com.invisiphoto;

import com.invisiphoto.data.DatabaseHelper;
import com.invisiphoto.utils.PrefsUtils;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class InvApplication extends Application {
	
	private PrefsUtils mPrefs;
	private SQLiteDatabase mDb;

	@Override
	public void onCreate() {
		super.onCreate();
		mPrefs = new PrefsUtils(this);
		DatabaseHelper dbHelper = new DatabaseHelper(this);
		mDb = dbHelper.getWritableDatabase();
	}
	
	public PrefsUtils getPrefs() {
		return mPrefs;
	}

	public static SQLiteDatabase getDb(Context context) {
		return ((InvApplication) context.getApplicationContext()).mDb;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		mDb.close();
	}

}