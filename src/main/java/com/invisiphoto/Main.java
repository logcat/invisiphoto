package com.invisiphoto;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.HttpResponseException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.invisiphoto.data.DownloadedImagesDbAdapter;
import com.invisiphoto.data.SavedImage;
import com.invisiphoto.data.SavedImagesDbAdapter;
import com.invisiphoto.fragments.TabsFragment;
import com.invisiphoto.imgur3.Imgur3Api;
import com.invisiphoto.imgur3.model.Image;
import com.invisiphoto.imgur3.Imgur3ApiFactory;
import com.invisiphoto.imgur3.model.ImageResponse;
import com.invisiphoto.utils.ImageUtils;
import com.invisiphoto.utils.PrefsUtils;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.android.view.ViewObservable;
import rx.schedulers.Schedulers;

import static com.invisiphoto.utils.ImageUtils.base64;
import static rx.android.view.ViewObservable.clicks;

public class Main extends Activity {

    private static final String NAME = "n";
    private static final String HASH = "h";
    private static final String DELETEHASH = "d";
    private static final String INTERVAL = "i";

    private static final String REDIRECT_BASE_URL = "http://dl.dropbox.com/u/13561399/static_html/invphoto/redirect.html";

    public static final int ACTION_TAKE_FROM_CAMERA = 1;
    public static final int ACTION_TAKE_FROM_GALLERY = 2;
    public static final int ACTION_TAKE_FROM_SECURE_VAULT = 3;

    public static final String METHOD_EMAIL = "email";
    public static final String METHOD_MESSAGING = "messaging";

    private static final String TAG = Main.class.getName();
    private Bitmap mBitmap;

    private Uri mSnapshotUri;
    private long mInterval;
    private String mSendMethod;

    private TabsFragment mTabsFragment;

    private Imgur3Api mImgur3Api;
    private DownloadedImagesDbAdapter mDownloadedImagesDbAdapter;
    private SavedImagesDbAdapter mSavedImagesDbAdapter;
    private PrefsUtils mPrefs;

    private SaveDialog mSaveDialog;
    private ProgressDialog mDialog;

    private ImageView mImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mPrefs = ((InvApplication) getApplication()).getPrefs();

        mInterval = 0;

        mDownloadedImagesDbAdapter = new DownloadedImagesDbAdapter(this);
        mSavedImagesDbAdapter = new SavedImagesDbAdapter(this);

        mImgur3Api = Imgur3ApiFactory.createApi();

        mSaveDialog = new SaveDialog(this);

        mDialog = new ProgressDialog(this);
        mDialog.setTitle("");

        mImage = (ImageView) findViewById(R.id.image);
    }

    public void onDeclinedTerms() {
        finish();
    }

    public void onAcceptedTerms() {
        mPrefs.setAccepted(true);
        mTabsFragment = (TabsFragment) getFragmentManager().findFragmentById(
                R.id.tabs_fragment);
        if (getIntent().getData() != null && "invphoto".equals(getIntent().getData().getScheme())) {
            Uri uri = getIntent().getData();
            String name = uri.getQueryParameter(NAME);
            final String hash = uri.getQueryParameter(HASH);
            final String deletehash = uri.getQueryParameter(DELETEHASH);
            String intervalString = uri.getQueryParameter(INTERVAL);
            mDialog.setMessage(getString(R.string.downloading_image));

            // check hash n database
            mDownloadedImagesDbAdapter.removeOutdatedImages();
            mDownloadedImagesDbAdapter.contains(hash)
                    .compose(applySchedulers())
                    .subscribe(contains -> {
                        if (contains) {
                            Log.d(TAG, "show image from database");
                            startViewer(hash);
                        } else {
                            mImgur3Api
                                    .getImageInfo(hash)
                                    .flatMap(imageResponse -> ImageUtils.downloadImage(imageResponse.data.link, Main.this))
                                    .flatMap(imageData -> mDownloadedImagesDbAdapter.saveToDb(hash, intervalString, imageData))
                                    .flatMap(bool -> mImgur3Api.imageDelete(deletehash))
                                    .compose(applySchedulers())
                                    .doOnError(error -> handleImageDownloadError(error))
                                    .subscribe(booleanResponse -> startViewer(hash));
                        }
                    });
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mPrefs.isAccepted()) {
            onAcceptedTerms();
        } else {
            final TermsDialog dialog = new TermsDialog(this);
            dialog.show();
            dialog.setCancelable(false);
        }
    }

    private void startViewer(final String hash) {
        Intent intent = new Intent(Main.this, ImageViewer.class);
        intent.putExtra(DownloadedImagesDbAdapter.HASH, hash);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case ACTION_TAKE_FROM_CAMERA:
                    setBitmapFromUri(mSnapshotUri);
                    break;
                case ACTION_TAKE_FROM_GALLERY:
                    setBitmapFromUri(data.getData());
                    break;
                case ACTION_TAKE_FROM_SECURE_VAULT:
                    Log.d(TAG, "gettings result");

                    String hash = data.getStringExtra(SavedImagesDbAdapter.HASH);
                    mSavedImagesDbAdapter.read(hash)
                            .compose(applySchedulers())
                            .subscribe(savedImage -> {
                                Glide.with(this).fromBytes().load(savedImage.data).into(mImage);
                                mTabsFragment.updateTabsState();
                            });
                    break;
                default:
                    break;
            }
        }
    }

    public void setBitmapFromUri(final Uri imageUri) {
        mDialog.setMessage(getString(R.string.downloading_image));
        mDialog.show();
        // TODO make dialog showing for image automatic via glide wrapper?
        Glide.with(Main.this).load(imageUri).asBitmap().into(new BitmapImageViewTarget(mImage) {
            @Override
            protected void setResource(Bitmap resource) {
                mBitmap = resource;
                mTabsFragment.updateTabsState();
                mDialog.dismiss();
                super.setResource(resource);
            }
        });
    }

    public static void showToast(int stringId, Context context) {
        Toast.makeText(context, stringId, Toast.LENGTH_SHORT).show();
    }

    public boolean doWeHaveImage() {
        return mBitmap != null;
    }

    public boolean doWehaveImageAndTime() {
        return mBitmap != null & mInterval != 0;
    }

    public void setSnapshotUri(Uri imageUri) {
        mSnapshotUri = imageUri;
    }

    public void setInterval(long interval) {
        mInterval = interval;
        mTabsFragment.updateTabsState();
    }

    public long getInterval() {
        return mInterval;
    }

    private void handleImageDownloadError(Throwable e) {
        // TODO handle it properly
        showError(R.string.error_unknown);
    }

    private void handleUploadError(Throwable e) {
        Log.e(TAG, "error", e);

        if (e instanceof HttpResponseException) {
            showError(R.string.error_no_photo);
        } else if (e instanceof IOException) {
            showError(R.string.error_connection);
        } else {
            showError(R.string.error_unknown);
        }
    }

    private void showError(int errorMessageId) {
        showToast(errorMessageId, Main.this);
    }

    private void handleSuccessUpload(ImageResponse response) {
        if (!response.success) {
            // TODO update messages
            showError(R.string.error_unknown);
        }

        final Map<String, String> values = takeValuesFromJsonResponse(response);
        final String customUrl = createCustomUrlFromParams(values);
        Log.d(TAG, "CustomUrl: " + customUrl);

        mSaveDialog.show();
        clicks(mSaveDialog.getButtonNo()).subscribe(onClickEvent -> {
            mSaveDialog.cancel();
            sendMessage(customUrl);
        });

        clicks(mSaveDialog.getButtonYes()).flatMap(onClickEvent -> {
            mSaveDialog.cancel();
            mDialog.setMessage(getString(R.string.saving_image));
            mDialog.show();
            return mSavedImagesDbAdapter.insert(
                    new SavedImage(values.get(HASH), ImageUtils.convertBitmapToByteArray(mBitmap)))
                    .compose(applySchedulers());
        }).subscribe((aVoid) -> {
            mDialog.cancel();
            sendMessage(customUrl);
        });
    }

    private void sendMessage(String customUrl) {
        Intent intent = new Intent();
        if (METHOD_MESSAGING.equals(mSendMethod)) {
            intent = createSendSMSIntent(customUrl);
        } else {
            intent = createSendEmailIntent(customUrl);
        }
        startActivity(intent);

        // clean up
        cleanApplicationToDefaultState();
    }

    public void uploadImage() {
        mDialog.setMessage(getString(R.string.uploading_image));
        mImgur3Api.imageUpload(base64(mBitmap))
                .compose(applySchedulers())
                .doOnError(error -> handleUploadError(error))
                .subscribe(response -> handleSuccessUpload(response));
    }

    private void cleanApplicationToDefaultState() {
        // mMessageIntent = null;
        mBitmap = null;
        mInterval = 0;
        mSendMethod = "";
        mImage.setImageResource(R.drawable.icon512);
        mTabsFragment.updateTabsState();
    }

    private Intent createSendEmailIntent(String customUrl) {
        Intent intent;
        intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "InvisiPhoto");
        String end = getString(R.string.click_to_view);
        intent.putExtra(
                Intent.EXTRA_TEXT,
                Html.fromHtml(new StringBuilder().append("<a href=\"").append(customUrl)
                        .append("\">Click here</a> ").append(end).toString()));
        return intent;
    }

    private Intent createSendSMSIntent(String customUrl) {
        Intent intent;
        Uri smsUri = Uri.parse("tel:");
        intent = new Intent(Intent.ACTION_VIEW, smsUri);
        intent.putExtra("sms_body", customUrl);
        intent.setType("vnd.android-dir/mms-sms");
        startActivity(intent);
        return intent;
    }

    @Deprecated
    protected String composeUri(String customUrl) {
        String end = getString(R.string.click_to_view);
        if (METHOD_EMAIL.equals(mSendMethod)) {
            return "mailto:?subject=InvisiPhoto&body=" + Uri.encode(customUrl) + " " + end;
        } else if (METHOD_MESSAGING.equals(mSendMethod)) {
            return "sms:?subject=InvisiPhoto&body=" + Uri.encode(customUrl);
        }
        return "";
    }

    private Spanned createHtmlBody(String customUrl) {
        String end = " " + getString(R.string.click_to_view);
        return Html.fromHtml("<a href=\"" + customUrl + "\">Click here</a>" + end);
    }

    private Map<String, String> takeValuesFromJsonResponse(ImageResponse response) {
        Map<String, String> result = new HashMap<String, String>();

        Image image = response.data;
        result.put(NAME, image.name);
        result.put(HASH, image.id);
        result.put(DELETEHASH, image.deletehash);
        result.put(INTERVAL, mInterval + "");

        return result;
    }

    public String createCustomUrlFromParams(Map<String, String> map) {
        String customUrl = REDIRECT_BASE_URL + "?";

        customUrl += "n=" + map.get(NAME);
        customUrl += "&h=" + map.get(HASH);
        customUrl += "&d=" + map.get(DELETEHASH);
        customUrl += "&i=" + map.get(INTERVAL);
        return customUrl;
    }

    public void setSendMethod(String sendMethod) {
        mSendMethod = sendMethod;
    }

    public void openSecureStore() {
        if (mPrefs.hasPassword()) {
            startActivityForResult(new Intent(this, PincodeChecker.class), ACTION_TAKE_FROM_SECURE_VAULT);
        } else {
            startActivityForResult(new Intent(this, SecureVault.class), ACTION_TAKE_FROM_SECURE_VAULT);
        }
    }

    final static Observable.Transformer<Object, Object> schedulersTransformer =
            observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<T, T> applySchedulers() {
        return (Observable.Transformer<T, T>) schedulersTransformer;
    }

}