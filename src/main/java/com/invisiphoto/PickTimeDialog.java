package com.invisiphoto;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import rx.android.view.ViewObservable;

import android.app.Dialog;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;

import static rx.android.view.ViewObservable.clicks;

public class PickTimeDialog extends Dialog {

	private Main mActivity;
	private WheelView mins;
	private WheelView hours;

	public PickTimeDialog(Main activity) {
		super(new ContextThemeWrapper(activity, android.R.style.Theme_Black));
		mActivity = activity;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pick_time_dialog);
		
		long defaultInterval = mActivity.getInterval();
		if (mActivity.getInterval() == 0) {
			defaultInterval = 60;
		}
		int minsValue = (int) defaultInterval / 60 % 60;
		int hoursValue = (int) defaultInterval / 3600 % 60;
		
		hours = (WheelView) findViewById(R.id.hour);
		hours.setViewAdapter(new NumericWheelAdapter(mActivity, 0, 23));
		hours.setCyclic(true);
		hours.setCurrentItem(hoursValue);
		hours.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				checkValuesAndScrollMinsIfNeeded();
			}
		});
		
		mins = (WheelView) findViewById(R.id.mins);
		mins.setViewAdapter(new NumericWheelAdapter(mActivity, 0, 59, "%02d"));
		mins.setCyclic(true);
		mins.setCurrentItem(minsValue);
		mins.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				checkValuesAndScrollMinsIfNeeded();
			}
		});

		clicks(findViewById(R.id.btn_cancel)).subscribe(onClickEvent -> cancel());
		clicks(findViewById(R.id.btn_settime)).subscribe(onClickEvent -> {
			long interval = hours.getCurrentItem() * 60 * 60 + mins.getCurrentItem() * 60;
			mActivity.setInterval(interval);
			cancel();
		});
	}
	
	private void checkValuesAndScrollMinsIfNeeded() {
		if (mins.getCurrentItem() == 0 && hours.getCurrentItem() == 0) {
			mins.setCurrentItem(1, true);
		} 
	}
}