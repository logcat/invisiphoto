package com.invisiphoto;


import android.app.Dialog;

@Deprecated
public class PickMessageMethodDialog extends Dialog {

//	private static final String TAG = PickMessageMethodDialog.class.getName();
//
//	public static final String PACKAGE_SMS = "com.android.mms"; 
//	private static final List<String> TESTED_LIST = new ArrayList<String>(Arrays.asList(PACKAGE_SMS, "com.android.email", "com.google.android.gm" ));
//	
	private Main mActivity;
//
//	private PackageManager mPackageManager;
//
//	private Intent mIntent;
//
	public PickMessageMethodDialog(Main activity) {
		super(activity);
		mActivity = activity;
	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		setContentView(R.layout.pick_message_method_dialog);
//		
//		mIntent = new Intent(Intent.ACTION_SEND);
//		mIntent.setType("text/plain");
//		mIntent.putExtra(Intent.EXTRA_SUBJECT, "subject");
//		mIntent.putExtra(Intent.EXTRA_TEXT, "text");
//
//		mPackageManager = mActivity.getPackageManager();
//
//		List<ResolveInfo> listInfo = mPackageManager.queryIntentActivities(
//				mIntent, 0);
//		Collections.sort(listInfo, sNameComparator);
//		
//		List<ResolveInfo> finalListInfo = new ArrayList<ResolveInfo>();
//		for (ResolveInfo resolveInfo : listInfo) {
//			if (TESTED_LIST.contains(resolveInfo.activityInfo.packageName)) {
//				finalListInfo.add(resolveInfo);
//			}
//		}
//		
//
//		final WheelView sendMethod = (WheelView) findViewById(R.id.send_method);
//		final SendMethodWheelAdapter adapter = new SendMethodWheelAdapter(
//				mActivity, finalListInfo);
//		sendMethod.setViewAdapter(adapter);
//
//		findViewById(R.id.btn_cancel).setOnClickListener(
//				new View.OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						cancel();
//					}
//				});
//
//		findViewById(R.id.btn_send).setOnClickListener(
//				new View.OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						Intent intent = new Intent(mIntent);
//						intent.setPackage(adapter.getResolveInfo(sendMethod
//								.getCurrentItem()).activityInfo.packageName);
//						
//						mActivity.setMessageIntent(intent);
//						mActivity.uploadImage();
//						
//						cancel();
//					}
//				});
//	}
//
//	private class SendMethodWheelAdapter extends AbstractWheelTextAdapter {
//
//		private List<ResolveInfo> mItems;
//
//		public SendMethodWheelAdapter(Context context, List<ResolveInfo> items) {
//			super(context);
//			mItems = items;
//		}
//
//		@Override
//		public View getItem(int index, View convertView, ViewGroup parent) {
//
//			ViewHolder holder;
//
//			if (convertView == null) {
//				convertView = new TextView(mActivity);
//
//				holder = new ViewHolder();
//				holder.text = (TextView) convertView;
//
//				holder.text.setTextColor(DEFAULT_TEXT_COLOR);
//				holder.text.setGravity(Gravity.CENTER_VERTICAL);
//				holder.text.setTextSize(DEFAULT_TEXT_SIZE);
//				holder.text.setLines(1);
//				holder.text.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
//				holder.text.setPadding(20, 0, 0, 0);
//
//				convertView.setTag(holder);
//			} else {
//				holder = (ViewHolder) convertView.getTag();
//			}
//			holder.text.setText(getItemText(index));
//
//			return convertView;
//		}
//
//		@Override
//		public int getItemsCount() {
//			return mItems.size();
//		}
//
//		@Override
//		protected CharSequence getItemText(int index) {
//			return getName(mItems.get(index));
//		}
//
//		public ResolveInfo getResolveInfo(int index) {
//			return mItems.get(index);
//		}
//	}
//
//	private CharSequence getName(ResolveInfo info) {
//		return mPackageManager
//				.getApplicationLabel(info.activityInfo.applicationInfo);
//	}
//
//	static class ViewHolder {
//		TextView text;
//	}
//
//	private final Comparator<ResolveInfo> sNameComparator = new Comparator<ResolveInfo>() {
//		public final int compare(ResolveInfo a, ResolveInfo b) {
//			CharSequence sa = getName(a);
//			CharSequence sb = getName(b);
//			return collator.compare(sa, sb);
//		}
//
//		private final Collator collator = Collator.getInstance();
//	};

}
