package com.invisiphoto;

import java.util.Arrays;
import java.util.List;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import rx.android.lifecycle.LifecycleObservable;
import rx.android.view.ViewObservable;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import static rx.android.view.ViewObservable.clicks;

public class PickMessageMethodDialog2 extends Dialog {

	private static final String TAG = PickMessageMethodDialog.class.getName();

	private Main mActivity;

	public PickMessageMethodDialog2(Main activity) {
		super(activity);
		mActivity = activity;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pick_message_method_dialog);

		SendMethod emailSendMethod = new SendMethod(mActivity.getString(com.invisiphoto.R.string.email), Main.METHOD_EMAIL);
		SendMethod smsSendMethod = new SendMethod(mActivity.getString(com.invisiphoto.R.string.messaging), Main.METHOD_MESSAGING);
		List<SendMethod> listMethod = Arrays.asList(new SendMethod[]{emailSendMethod, smsSendMethod});
		
		final WheelView sendMethods = (WheelView) findViewById(R.id.send_method);
		final SendMethodWheelAdapter adapter = new SendMethodWheelAdapter(
				mActivity, listMethod);
		sendMethods.setViewAdapter(adapter);

		clicks(findViewById(R.id.btn_cancel)).subscribe(onClickEvent -> cancel());

		clicks(findViewById(R.id.btn_send)).subscribe(onClickEvent -> {
			String sendMethod = adapter.getSendMethod(sendMethods.getCurrentItem()).sendMethod;
			mActivity.setSendMethod(sendMethod);
			mActivity.uploadImage();
			cancel();
		});
	}

	private class SendMethod {
		public String name;
		public String sendMethod;

		private SendMethod(String name, String scheme) {
			this.name = name;
			this.sendMethod = scheme;
		}
	}

	private class SendMethodWheelAdapter extends AbstractWheelTextAdapter {

		private List<SendMethod> mItems;

		public SendMethodWheelAdapter(Context context, List<SendMethod> items) {
			super(context);
			mItems = items;
		}

		@Override
		public View getItem(int index, View convertView, ViewGroup parent) {

			ViewHolder holder;

			if (convertView == null) {
				convertView = new TextView(mActivity);

				holder = new ViewHolder();
				holder.text = (TextView) convertView;

				holder.text.setTextColor(DEFAULT_TEXT_COLOR);
				holder.text.setGravity(Gravity.CENTER_VERTICAL);
				holder.text.setTextSize(DEFAULT_TEXT_SIZE);
				holder.text.setLines(1);
				holder.text.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
				holder.text.setPadding(20, 0, 0, 0);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.text.setText(getItemText(index));

			return convertView;
		}

		@Override
		public int getItemsCount() {
			return mItems.size();
		}

		@Override
		protected CharSequence getItemText(int index) {
			return mItems.get(index).name;
		}
		
		public SendMethod getSendMethod(int index) {
			return mItems.get(index);
		}
	}

	static class ViewHolder {
		TextView text;
	}
}