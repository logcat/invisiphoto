package com.invisiphoto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.widget.TextView;

import com.invisiphoto.utils.PrefsUtils;
import com.invisiphoto.widget.Pin;
import com.invisiphoto.widget.Pin.OnPinInputDoneListener;
import com.invisiphoto.widget.ShakeListener;

public class PincodeChecker extends Activity {

	private PrefsUtils mPrefs;
	private Pin mPin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pincode_cheker);

		((TextView) findViewById(R.id.header)).setText(R.string.password);

		mPrefs = ((InvApplication) getApplication()).getPrefs();

		mPin = (Pin) findViewById(R.id.pin);
		mPin.setActivityToFinishOnBack(this);

		mPin.setOnPinInputDoneListener(new OnPinInputDoneListener() {
			@Override
			public void onPinInputDone(final Pin pin, String text) {
				if (mPrefs.matchPassword(text)) {
					startActivityForResult(new Intent(PincodeChecker.this,
							SecureVault.class),
							Main.ACTION_TAKE_FROM_SECURE_VAULT);
				} else {
					pin.shake(new ShakeListener() {
						@Override
						public void onAnimationEnd(Animation animation) {
							pin.clearText();
						}
					});
				}
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mPin.showKeyboard();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mPin.hideKeyboard();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case Main.ACTION_TAKE_FROM_SECURE_VAULT:
				setResult(RESULT_OK, data);
				break;
			default:
				break;
			}
		}
		finish();
	}
}