package com.invisiphoto;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import rx.android.view.ViewObservable;

import static rx.android.view.ViewObservable.clicks;

public class TermsDialog extends Dialog {

	private final Main mActivity;

	public TermsDialog(Main activity) {
		super(activity);
		mActivity = activity;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.terms);		
		
		((TextView) findViewById(R.id.header)).setText(R.string.terms_of_use);
		
		TextView terms = (TextView) findViewById(R.id.terms_text);
		String termsText = getContext().getResources().getString(R.string.terms_text);
		terms.setText(termsText);
		
		clicks(findViewById(R.id.btn_accept)).subscribe(onClickEvent -> {
			cancel();
			mActivity.onAcceptedTerms();
		});

		clicks(findViewById(R.id.btn_decline)).subscribe(onClickEvent -> {
			cancel();
			mActivity.onDeclinedTerms();
		});
	}
}