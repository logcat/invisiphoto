package com.invisiphoto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.invisiphoto.data.DownloadedImage;
import com.invisiphoto.data.DownloadedImagesDbAdapter;

import static com.invisiphoto.Main.applySchedulers;

public class ImageViewer extends FragmentActivity {

	private static final String TAG = ImageViewer.class.getSimpleName();
	
	private ImageView mView;
	private DownloadedImage mImage;
	private DownloadedImagesDbAdapter dbAdapter;
	private RelativeLayout mContainer;
	private Timer mTimer;
	private TextView mTimeView;

	private SimpleDateFormat mDateFormat;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.image_viewer);
		
		((TextView) findViewById(R.id.header)).setText(R.string.photo);
		
		dbAdapter = new DownloadedImagesDbAdapter(this);
		
		mView = (ImageView) findViewById(R.id.photo);
		mContainer = (RelativeLayout) findViewById(R.id.container);
		mContainer.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return mView.onTouchEvent(event);
			}
		});

		mTimeView = (TextView) findViewById(R.id.time);
		
		mDateFormat = new SimpleDateFormat("HH:mm:ss"); 
		mDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+00"));
		
		mTimer = new Timer();
		dbAdapter.read(getIntent().getStringExtra(DownloadedImagesDbAdapter.HASH))
				.compose(applySchedulers())
				.subscribe(downloadedImage -> {
					mImage = downloadedImage;
					mView.setImageBitmap(mImage.getBitmap());
					scheduleTimer();
				});
	}

	private void scheduleTimer() {
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						long remainingTime = mImage.interval + mImage.timeCreated - System.currentTimeMillis();
						if (remainingTime > 0) {
							mTimeView.setText(mDateFormat.format(remainingTime));
						} else {
							finish();
						}
					}
				});
			}
		}, new Date(System.currentTimeMillis()), 1000);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mTimer.cancel();
		finish();
	}
}